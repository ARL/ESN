<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', function () {
    return view('welcome');
})->middleware('guest');

// Authentification
Route::auth();


// Admin routes
Route::group([
	'prefix' => config('backpack.base.route_prefix'), 
	'middleware' => ['web', 'auth', 'auth.admin'],
	'namespace'  => '\Backpack\Base\app\Http\Controllers'], function () {
            
	    Route::get('dashboard', '\Backpack\Base\app\Http\Controllers\AdminController@dashboard');
		\CRUD::resource('permission', '\Backpack\PermissionManager\app\Http\Controllers\PermissionCrudController');
		\CRUD::resource('role', '\Backpack\PermissionManager\app\Http\Controllers\RoleCrudController');
		\CRUD::resource('user', '\Backpack\PermissionManager\app\Http\Controllers\UserCrudController');
});

// Home on login
Route::get('home', 'HomeController@index');

// Profile
Route::get('profile/avatar/edit', 'User\UserAvatarController@edit');
Route::post('profile/avatar/edit', 'User\UserAvatarController@update');


Route::get('wall', 'ProfileController@index');
Route::get('about', 'ProfileController@about');
Route::get('friend', 'Friend\FriendController@index');
Route::get('events', 'EventController@indexFutur');
Route::get('eventsPast', 'EventController@indexPast');
Route::post('events', 'EventController@store');
// Route::get('messages', 'Message\MessagesController@index');
Route::get('albums', 'Album\AlbumsController@index');
Route::get('albums/create', 'Album\AlbumsController@create');
Route::get('albums/{id}', 'Album\AlbumsController@show')->where('id', '[0-9]+');


Route::get('wall/{id}', 'ProfileController@show')->where('id', '[0-9]+');
Route::get('wall/{id}/about', 'ProfileController@aboutSomeone')->where('id', '[0-9]+');
Route::get('wall/{id}/friend', 'ProfileController@friendSomeone')->where('id', '[0-9]+');
Route::get('wall/{id}/events', 'ProfileController@eventsSomeone')->where('id', '[0-9]+');
Route::get('wall/{id}/albums', 'ProfileController@albumsSomeone')->where('id', '[0-9]+');



Route::get('profile/edit', 'ProfileController@edit');
Route::post('profile/update', 'ProfileController@update');

// Recherche
Route::get('users', 'User\UserController@fetch');

// Demande ami
// Route::get('friend', 'Friend\FriendController@index');
Route::get('friend/store/{id}', 'Friend\FriendController@store')->where('id', '[0-9]+');
Route::get('friend/store/{id}/accept', 'Friend\FriendController@accept')->where('id', '[0-9]+');
Route::get('friend/store/{id}/deny', 'Friend\FriendController@deny')->where('id', '[0-9]+');
Route::get('friend/store/{id}/remove', 'Friend\FriendController@remove')->where('id', '[0-9]+');
Route::get('friend/store/{id}/block', 'Friend\FriendController@block')->where('id', '[0-9]+');
Route::get('friend/store/{id}/unblock', 'Friend\FriendController@unblock')->where('id', '[0-9]+');
Route::get('friend/requests', 'Friend\FriendRequestController@index');

// Events
Route::get('events/{id}', 'EventController@show')->where('id', '[0-9]+');
Route::get('events/create', 'EventController@create');


// Posts
Route::post('posts', 'Post\PostsController@store');
Route::get('posts/{id}', 'Post\PostsController@show')->where('id', '[0-9]+');
Route::get('posts/delete/{id}', 'Post\PostsController@delete');
Route::get('posts/{id}/reaction/{reaction}', 'Reaction\ReactionsController@store')->where('id', '[0-9]+');

// Comments
// Route::post('comments', 'Comment\CommentsController@store');

// Messages
Route::get('messages', 'Message\MessagesController@index');
