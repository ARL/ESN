<?php

return [

    'tables' => [
        'fr_pivot' => 'friendships',
        'fr_groups_pivot' => 'user_friendship_groups'
    ],

    'groups' => [
        'acquaintances' => 0,
        'close_friends' => 1,
        'family' => 2,
        'ing1' => 3,
        'ing2' => 4,
        'ing3' => 5,
        'ing4' => 6,
        'ing5' => 7,
    ]

];