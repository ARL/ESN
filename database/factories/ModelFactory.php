<?php

use \Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/**
 * Creations of Users
 */
$factory->define(App\User::class, function (Faker\Generator $faker) {

	$localisedFaker = Faker\Factory::create("fr_FR");

    return [
        'firstname' => $localisedFaker->firstName(),
        'lastname' => $localisedFaker->lastName,
        'username'=> $localisedFaker->unique()->userName,
        'address' => $localisedFaker->address,
        'email' => $localisedFaker->unique()->safeEmail,
        'birthday' => $localisedFaker->date(),
        'password' => bcrypt('Secret0'),
        'remember_token' => str_random(10),
    ];
});




/** @var \Illuminate\Database\Eloquent\Factory $factory */
/**
 * Creations of Medias
 */
$factory->define(App\Models\Media::class, function (Faker\Generator $faker) {
	$localisedFaker = Faker\Factory::create("fr_FR");
	

	//To turn a timestamp into a string, you can use date(), ie:
	$date1 = \Carbon\Carbon::instance($localisedFaker->dateTime());
	$date2 = $date1->addMinutes(1080);

    $words = $localisedFaker->words(5);
    $path = "";
    for($i=0;$i<5;$i++)
        $path= $path."/".$words[$i];

	return [
        'type' => $localisedFaker->mimeType,
        'path' => $path.".".$localisedFaker->fileExtension,
        'user_id'=> rand(1,20),
    ];

});

/**
 * Creations of Event
 */
$factory->define(App\Models\Event::class, function (Faker\Generator $faker) {
	$localisedFaker = Faker\Factory::create("fr_FR");

	//To turn a timestamp into a string, you can use date(), ie:
	$startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('-30 days', '+30 days')->getTimestamp());
    $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $startDate)->addHour();

	return [
        'beginDate' => $startDate,
        'endDate' => $endDate,
        'title' => $localisedFaker->sentence(6),
        'address' => $localisedFaker->sentence(6),
        'description' => $localisedFaker->text(300),
        'media_id' => rand(1,20),
        'user_id' => rand(1,20),
        'visibility_id'=> rand(1,3),

    ];

});

/**
 * Creations of Message
 */
$factory->define(App\Models\Message::class, function (Faker\Generator $faker) {
    $localisedFaker = Faker\Factory::create("fr_FR");

    return [
        'content' => $localisedFaker->text(200),
        'group_id'=> rand(1,7),
        'user_id' => rand(1,20),

    ];

});

/**
 * Creations of Post
 */
$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {
    $localisedFaker = Faker\Factory::create("fr_FR");

    return [
        'user_id' => rand(1,50),
        'visibility_id' => rand(1,3),
        'media_id' => rand(1,20),
        'place' => $localisedFaker->city,
        'mood' => $localisedFaker->randomElement(['très bien', 'triste', 'heureux', 'hahaha', 'vraiment triste']),
        'activity' => $localisedFaker->randomElement(['en train de jouer', 'en train de pleurer de rire', 'en train de me promener']),
        'content' => $localisedFaker->text(800),
        'date' =>  Carbon::createFromTimeStamp($faker->dateTimeBetween('-30 days', '+30 days')->getTimestamp()),
    ];

});


/**
 * Creations of EventGroup
 */$factory->define(App\Models\EventGroup::class, function (Faker\Generator $faker) {
    
    return [
        'group_id'=> rand(1,8),
        'event_id'=> rand(1,30),
    ];
});

/**
 * Creations of UserGroup
 */
$factory->define(App\Models\UserGroup::class, function (Faker\Generator $faker) {
    
    return [
        'group_id'=> rand(1,8),
        'user_id'=> rand(1,50),
    ];
});


/**
 * Creations of PostGroup
 */
$factory->define(App\Models\PostGroup::class, function (Faker\Generator $faker) {
    
    return [
        'group_id'=> rand(1,8),
        'post_id'=> rand(1,20),
    ];
});

/**
 * Creations of Reaction
 */
$factory->define(App\Models\Reaction::class, function (Faker\Generator $faker) {
    $localisedFaker = Faker\Factory::create("fr_FR");

    return [
        'name' => $localisedFaker->randomElement(['LIKE', 'LOVE', 'SAD', 'HAHA']),
        'post_id'=> rand(1,20),
        'user_id' => rand(1,20),

    ];

});

/**
 * Creations of Comment
 */
$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {
    $localisedFaker = Faker\Factory::create("fr_FR");

    return [
        'content' => $localisedFaker->text(500),
        'post_id'=> rand(1,20),
        'user_id' => rand(1,20),

    ];

});

/**
 * Creations of Albums
 */
$factory->define(App\Models\Album::class, function (Faker\Generator $faker) {
    $localisedFaker = Faker\Factory::create("fr_FR");

    return [
        'title' => $localisedFaker->text(50),
        'description' => $localisedFaker->text(500),
        'user_id' => rand(1,20),

    ];

});

/**
 * Settings Media in Album
 */
$factory->define(App\Models\AlbumMedia::class, function (Faker\Generator $faker) {
    $localisedFaker = Faker\Factory::create("fr_FR");

    return [
        'album_id' => rand(1,15),
        'media_id' => rand(1,20),
    ];

});

