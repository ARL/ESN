<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_medias', function (Blueprint $table) {
            $table->increments('id');
        
            $table->integer('album_id')->unsigned();
            $table->integer('media_id')->unsigned();
            $table->timestamps();

            $table->unique(['album_id', 'media_id']);
            $table->foreign('album_id')
                    ->references('id')
                    ->on('albums')
                    ->onDelete('cascade');

             $table->foreign('media_id')
                    ->references('id')
                    ->on('medias')
                    ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_medias');
    }
}
