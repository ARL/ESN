<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediasTable extends Migration
{
    /**
     * Run the migrations of medias.
     *
     * @return void
     */
    public function up(){
        Schema::create('medias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('type');
            $table->string('path')->unique();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->unique(['user_id', 'path'] );
       });

        Schema::table('users', function(Blueprint $table){


             $table->foreign('avatar_id')
                    ->references('id')
                    ->on('medias')
                    ->onDelete('cascade');
            
            $table->foreign('avatarResized_id')
                    ->references('id')
                    ->on('medias')
                    ->onDelete('cascade');

            $table->foreign('cover_id')
                    ->references('id')
                    ->on('medias')
                    ->onDelete('cascade');
            
            $table->foreign('coverResized_id')
                    ->references('id')
                    ->on('medias')
                    ->onDelete('cascade');
        });
    }

   /**
     * Reverse the migrations. Delete media foreign key and users foreign keys
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medias', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['avatar_id']);
            $table->dropForeign(['avatarResized_id']);
            $table->dropForeign(['cover_id']);
            $table->dropForeign(['coverResized_id']);

        });

        
    }
}
