<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations of posts.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('visibility_id')->unsigned()->default(1);
            $table->integer('media_id')->unsigned()->nullable();
            $table->string('place')->nullable();
            $table->string('mood')->nullable();
            $table->string('activity')->nullable();
            $table->text('content')->nullable();
            $table->dateTime('date')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('visibility_id')
                ->references('id')
                ->on('visibilities')
                ->onDelete('cascade'); 

            $table->foreign('media_id')
                ->references('id')
                ->on('medias')
                ->onDelete('cascade');  

            
        });

    }

    /**
     * Reverse the migrations by dropping foreign key and table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
             $table->dropForeign(['user_id']);
             $table->dropForeign(['visibility_id']);
             $table->dropForeign(['media_id']);
             

        });
        Schema::dropIfExists('posts');
    }
}
