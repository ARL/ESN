<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations for creating table users
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('avatar_id')->unsigned()->nullable();
            $table->integer('avatarResized_id')->unsigned()->nullable();
            $table->integer('cover_id')->unsigned()->nullable();
            $table->integer('coverResized_id')->unsigned()->nullable();
            $table->string('username')->unique()->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('cellphone', 12)->nullable();
            $table->dateTime('birthday')->nullable();
            $table->string('gender')->nullable();
            $table->string('address')->nullable();
            $table->unsignedTinyInteger('currentYear')->default(1);
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('medias');
        Schema::dropIfExists('users');
    }
}
