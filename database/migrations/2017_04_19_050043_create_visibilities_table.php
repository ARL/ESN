<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisibilitiesTable extends Migration
{
    /**
     * Run the migration of visibilities
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visibilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();

            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations by dropping event foreign key and table visibilities.
     *
     * @return void
     */
    public function down()
    {

        Schema::hasTable('events', function(Blueprint $table){
              $table->dropForeign(['visibility_id']);
        });
        Schema::dropIfExists('visibilities');
    }
}
