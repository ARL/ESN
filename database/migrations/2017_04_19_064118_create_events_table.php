<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{

    /**
     * Run the migration of events.
     *
     * @return void
     */
    public function up(){
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('visibility_id')->unsigned()->default(1);
            $table->integer('media_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('address')->nullable();  
            $table->dateTime('beginDate')->nullable(); 
            $table->dateTime('endDate')->nullable();   
            $table->text('description')->nullable();
            $table->timestamps();
            
        });

        Schema::table('events', function (Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('media_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->foreign('visibility_id')
                    ->references('id')
                    ->on('visibilities')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}