<?php

use Illuminate\Database\Seeder;
use \App\Models\UserGroup;

class UserGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UserGroup::class, 100)->create();
    }
}
