<?php

use Illuminate\Database\Seeder;
use \App\Models\AlbumMedia;

class AlbumMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void call generation of 50 AlbumMedia using fil ModelFactory
     */
    public function run()
    {
        factory(AlbumMedia::class, 50)->create();
    }
}
