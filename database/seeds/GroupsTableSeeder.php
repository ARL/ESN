<?php

use Illuminate\Database\Seeder;
use \App\Models\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
        	'name' => "iTeam",
    	]);

        for($i=2017; $i<2022; $i++)
	        Group::create([
	        	'name' => "promo".$i,
	    	]);

        Group::create([
        	'name' => "Code::status(200)",
    	]);

    	Group::create([
        	'name' => "Majeur SE",
    	]);
    }
}
