<?php

use Illuminate\Database\Seeder;
use \App\Models\Reaction;

class ReactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Reaction::class, 20)->create();
    }
}
