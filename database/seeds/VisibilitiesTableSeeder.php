<?php

use Illuminate\Database\Seeder;
use \App\Models\Visibility;

class VisibilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
        Visibility::create([
        	'name' => "public",
    	]);

        Visibility::create([
        	'name' => "private",
    	]);

        Visibility::create([
        	'name' => "onlyFor",
    	]);

      
    }
}
