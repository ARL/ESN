<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       /**
        * Tell the order of table completions
        */
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(VisibilitiesTableSeeder::class);
        $this->call(MediasTableSeeder::class);    
        $this->call(GroupsTableSeeder::class);  
        $this->call(MessagesTableSeeder::class);  
        $this->call(PostsTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        try{
            $this->call(EventGroupTableSeeder::class);
        }catch(Exception $e){}
        
        try{
            $this->call(UserGroupTableSeeder::class);
        }catch(Exception $e){}
        try{
            $this->call(PostGroupTableSeeder::class);
        }catch(Exception $e){}

        try{
            $this->call(ReactionsTableSeeder::class);
        }catch(Exception $e){}


        try{
            $this->call(CommentsTableSeeder::class);
        }catch(Exception $e){}

        try{
            $this->call(AlbumsTableSeeder::class);
        }catch(Exception $e){}
        try{
            $this->call(AlbumMediaTableSeeder::class);
        }catch(Exception $e){}


           
    }
}
