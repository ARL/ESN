<?php

use Illuminate\Database\Seeder;
use \App\User;
use \App\Models\Event;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$admin = User::create([
        	'firstname' => 'Super',
        	'lastname' => 'Admin',
            'username' => 'admin',
        	'email' => 'admin@edu.ece.fr',
        	'birthday' => \Carbon\Carbon::now(),
        	'created_at' => \Carbon\Carbon::now(),
        	'updated_at' => \Carbon\Carbon::now(),
        	'password' => bcrypt('SecretAdmin1'),
    	]);

        $admin->assignRole('admin');

        // Création de 50 utilisateurs normaux
        factory(User::class, 50)->create()->each(function ($u) {
        	$u->assignRole('author');
        });
    }
}
