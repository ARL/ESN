<?php

use Illuminate\Database\Seeder;
use \App\Models\EventGroup;

class EventGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EventGroup::class, 100)->create();
    }
}
