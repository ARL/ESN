@extends('layouts.app')

@section('content')

	@include('layouts.left-sidenav')
	{{-- Code HTML à partir d'ici --}}

	<div class="col-sm-6">
		<h1>Mes albums</h1>
		<a href="{{url('albums/create')}}" ><i class="fa fa-plus-circle fw" aria-hidden="true" ></i>&nbsp; Nouveau</a>

		<ul class="list-group">
			<div class="row">
				@foreach ($albums as $album)
					<div class="col-sm-4" style="padding-bottom:10px">
						<li class="list-group-item">
							<div class="justify-content-between">
								<a class="card-title mb-1" href="{{ url('albums/'.$album->id) }}"> <h2 style="color:black"> {{ $album->title }} </h2></a>
								<p class="text-muted" style="font-size:15px"> {{ $album->description }} </p>
								<!-- {{url('albums/'.$album->id.'/remove')}} -->
								<a class="btn btn-danger" href="#" role="button">Supprimer</a>
								
							</div>
						</li>
					</div>
				@endforeach
			</div>
		</ul>
	</div>

	@include('layouts.right-content')

@endsection