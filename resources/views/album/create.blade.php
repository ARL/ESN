@extends('layouts.app')

@section('content')

	@include('layouts.left-sidenav')
	{{-- Code HTML à partir d'ici --}}
	<div style="padding:10px 10px 0px 10px">
		<h1>Création d'un nouvel album</h1>

		<div class="card" style="border:1px solid #cccccc; border-top-right-radius:0.25rem; border-top-left-radius:0.25rem; padding:10px 10px 0px 10px">

			<div class="container">
			  	<form  method="POST" action="{{url('events')}}" enctype="multipart/form-data" >
			  		{{!! csrf_field() !!}}
				    <div class="form-group row">
				      <label for="title" class="col-sm-2 col-form-label">Title : </label>
				      <div class="col-sm-10">
				        <input type="text" class="form-control" name="title" placeholder="Mon titre">
				      </div>
				    </div>
				    <div class="form-group row">
				      <label for="content" class="col-sm-2 col-form-label">Description : </label>
				      <div class="col-sm-10">
				        <textarea class="form-control" name="content" placeholder="Ma description" data-autoresize ></textarea>
				      </div>
				    </div>
				    <div class="form-group row">
						<div class="offset-sm-2 col-sm-10">
					        <button onclick="ajouter()" class="btn btn-primary">Ajouter une media</button>
					    </div>
					</div>
				    <div class="form-group row">
				      <label for="media" class="col-sm-2 col-form-label">Photo : </label>
				      <div class="col-sm-10">
				        <input type="file" class="form-control" name="media"  accept="image/*" >
				      </div>
				    </div>
				    
				    <div class="form-group row">
						<div class="offset-sm-2 col-sm-10">
					        <button type="submit" class="btn btn-primary">Créer</button>
					    </div>
					</div>
			    
	            </div>
				
			</form>
		</div>
	</div>
	</div>

	@include('layouts.right-content')

@endsection