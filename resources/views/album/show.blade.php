@extends('layouts.app')

@section('content')

	@include('layouts.left-sidenav')
	{{-- Code HTML à partir d'ici --}}
	<div class="col-sm-6">
		<div class="row">
			<li class="list-group-item">
				<div class="justify-content-between">
					<h2 style="color:black"> {{ $album->title }} </h2>
					<p class="text-muted" style="font-size:15px"> {{ $album->description }} </p>
					<a class="btn btn-danger" href="#" role="button">Supprimer</a>
				</div>
			</li>
		</div>

		<div class="row">
			@foreach ($medias as $media)
				<div class="col-sm-5" style="padding:10px; margin:10px; border:1px solid black">
					<p> {{$media->path}} </p>
				</div>
			@endforeach
		</div>
	</div>

	@include('layouts.right-content')

@endsection