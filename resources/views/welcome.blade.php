@extends('layouts.offline')

@section('content')
    <h1 class="cover-heading">ECE Social Network</h1>
    <p class="lead">Blablabla de pourquoi ce site est génial et il devrait être déployé sur Campus.</p>
    <p class="lead">
        <a href="{{ route('register') }}" class="btn btn-lg btn-secondary">S'inscrire maintenant</a>
    </p>
@endsection