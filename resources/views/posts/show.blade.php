@extends('layouts.app')

@section('content')

    @include('layouts.left-sidenav')
    <div class="col-sm-6">
        @include('layouts.post')
    </div>
    @include('layouts.right-content')

@endsection