@extends('layouts.offline')
@section('loginActive', 'active')
@section('content')
<h1 class="cover-heading">Connexion</h1>
<div class="container">
    <div class="row">
        <form role="form" class="center-form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-form-label sr-only">Adresse email</label>

                <div class="col-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Adresse email" required autofocus>

                    @if ($errors->has('email'))
                        <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>

            <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-form-label sr-only">Mot de passe</label>

                <div class="col-12">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Mot de passe" required>

                    @if ($errors->has('password'))
                        <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col-12">
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Se souvenir de moi</span>
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">
                        Se connecter
                    </button>

                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Mot de passe oublié ?
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
