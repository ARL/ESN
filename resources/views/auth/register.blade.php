@extends('layouts.offline')
@section('registerActive', 'active')
@section('content')
<h1 class="cover-heading">Inscription</h1>
<div class="container">
    <div class="row">
        <form role="form" class="center-form m350" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="form-group row{{ $errors->has('firstname') ? ' has-danger' : '' }}">
                <label for="firstname" class="col-form-label sr-only">Prénom</label>

                <div class="col-12">
                    <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="Prénom" required autofocus>

                    @if ($errors->has('firstname'))
                        <div class="form-control-feedback">{{ $errors->first('firstname') }}</div>
                    @endif

                </div>
            </div>

            <div class="form-group row{{ $errors->has('lastname') ? ' has-danger' : '' }}">
                <label for="lastname" class="col-form-label sr-only">Nom</label>

                <div class="col-12">
                    <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="Nom" required>

                    @if ($errors->has('lastname'))
                        <div class="form-control-feedback">{{ $errors->first('lastname') }}</div>
                    @endif
                </div>
            </div>

            <div class="form-group row{{ $errors->has('birthday') ? ' has-danger' : '' }}">
                <label for="birthday" class="col-form-label sr-only">Date de naissance</label>

                <div class="col-12">
                    <input id="birthday" type="text" class="form-control" onfocus="(this.type='date')" onblur="(this.type='text')" name="birthday" value="{{ old('birthday') }}" placeholder="Date de naissance" required>

                    @if ($errors->has('birthday'))
                        <div class="form-control-feedback">{{ $errors->first('birthday') }}</div>
                    @endif
                </div>
            </div>

            <div class="form-group row{{ $errors->has('email') ? ' has-danger' : '' }}">
                <label for="email" class="col-form-label sr-only">Email</label>

                <div class="col-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>

                    @if ($errors->has('email'))
                        <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>

            <div class="form-group row{{ $errors->has('password') ? ' has-danger' : '' }}">
                <label for="password" class="col-form-label sr-only">Mot de passe</label>

                <div class="col-12">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Mot de passe" required>

                    @if ($errors->has('password'))
                        <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm" class="col-form-label sr-only">Confirmation</label>

                <div class="col-12">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmation du mot de passe" required>
                </div>
            </div>

            <div class="form-group row{{ $errors->has('gender') ? ' has-danger' : '' }}">
                <div class="col-12">
                    <label class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" name="gender" value="male" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Homme</span>
                    </label>
                    <label class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" name="gender" value="female">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Femme</span>
                    </label>
                </div>
                @if ($errors->has('gender'))
                    <div class="form-control-feedback">{{ $errors->first('gender') }}</div>
                @endif
            </div>

            <div class="form-group row">
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">
                        S'inscrire
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
