@extends('layouts.app')

@section('content')

	@include('layouts.left-sidenav')
	{{-- Code HTML à partir d'ici --}}
	<div class="col-sm-6">
		<ul class="list-group">
			@foreach ($senders as $user)
				<li class="list-group-item">
					<img src="http://placehold.it/120x120" style="border:1px solid black; margin:5px 15px 5px 5px">
					<div class="justify-content-between">
						<h2 class="card-title mb-1"> {{ $user->firstname }} {{ $user->lastname }} </h2>
						<p class="text-muted" style="font-size:15px"> {{ $user->getMutualFriendsCount(Auth::user() )}} amis en commun </p>
						
						<a class="btn btn-primary" href="{{url('friend/store/'.$user->id.'/accept')}}" role="button">Accepter</a>
						<a class="btn btn-danger" href="{{url('friend/store/'.$user->id.'/deny')}}" role="button">Refuser</a>
					</div>
				</li>
			@endforeach
		</ul>
	</div>

	@include('layouts.right-content')

@endsection