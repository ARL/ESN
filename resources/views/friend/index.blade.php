@extends('layouts.app')

@section('content')

	@include('layouts.left-sidenav')
	{{-- Code HTML à partir d'ici --}}
	<div class="col-sm-6">
		<ul class="list-group">
			<div class="row">
				@foreach ($friends as $user)
					<div class="col-sm-6" style="padding-bottom:10px">
						<li class="list-group-item">
							<img class="card-img-left" src="http://placehold.it/120x120">
							<div class="card-block" style="clear:both; float:right" >
								<h2 style="overflow:hidden; text-overflow:ellipsis; white-space:nowrap; max-width:232px; max-height:35px" data-toggle="tooltip" data-placement="top" title="{{ $user->firstname }} {{ $user->lastname }}">{{ $user->firstname }} {{ $user->lastname }}</h2>
								<p class="text-muted" style="font-size:15px"> {{ $user->getFriendsCount()}} amis</p>

								<a class="btn btn-warning btn-sm" href="{{url('friend/store/'.$user->id.'/remove')}}" role="button">Retirer des amis</a>
								<a class="btn btn-danger btn-sm" href="{{url('friend/store/'.$user->id.'/block')}}" role="button">Bloquer</a>
							</div>
						</li>
					</div>
				@endforeach
			</div>
		</ul>
	</div>

	@include('layouts.right-content')

@endsection