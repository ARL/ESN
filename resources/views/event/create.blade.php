@extends('layouts.app')

@section('content')

	@include('layouts.left-sidenav')
	{{-- Code HTML à partir d'ici --}}
	<div style="padding:10px 10px 0px 10px">
		<h1>Création d'un nouvel événement</h1>

		<div class="card" style="border:1px solid #cccccc; border-top-right-radius:0.25rem; border-top-left-radius:0.25rem; padding:10px 10px 0px 10px">

			<div class="container">
			  	<form  method="POST" action="{{url('events')}}" enctype="multipart/form-data" >
			  		{{!! csrf_field() !!}}
				    <div class="form-group row">
				      <label for="title" class="col-sm-2 col-form-label">Title : </label>
				      <div class="col-sm-10">
				        <input type="text" class="form-control" name="title" placeholder="Mon titre">
				      </div>
				    </div>
				    <div class="form-group row">
				      <label for="content" class="col-sm-2 col-form-label">Contenu : </label>
				      <div class="col-sm-10">
				        <textarea class="form-control" name="content" placeholder="Mon contenu" data-autoresize ></textarea>
				      </div>
				    </div>
				    <div class="form-group row">
				      <label for="media" class="col-sm-2 col-form-label">Photo : </label>
				      <div class="col-sm-10">
				        <input type="file" class="form-control" name="media"  accept="image/*" >
				      </div>
				    </div>
				    <div class="form-group row">
				      <label for="address" class="col-sm-2 col-form-label">Lieu :</label>
				      <div class="col-sm-10">
				        <input type="text" class="form-control" name="address" placeholder="">
				      </div>
				    </div>
				    <div class="form-group row">
				      <label for="bengiDate" class="col-sm-2 col-form-label">Date de début :</label>
				      <div class="col-sm-10">
				        <input type="datetime-local" class="form-control" name="beginDate" required>
				      </div>
				    </div>
				    <div class="form-group row">
				      <label for="endDate" class="col-sm-2 col-form-label">Date de fin :</label>
				      <div class="col-sm-10">
				        <input type="datetime-local" class="form-control" name="endDate">
				      </div>
				    </div>
				    <fieldset class="form-group row">
				      <legend class="col-form-legend col-sm-2" style="max-width: 30%">Rendre cet événement :</legend>
				      <div class="col-sm-10" style="margin-left:3%">
				        <div class="form-check form-check-inline">
				          <label class="form-check-label">
				            <input class="form-check-input" type="radio" name="visibility" id="1" value="1" checked>
				            Public
				          </label>
				        </div>
				        <div class="form-check form-check-inline">
				          <label class="form-check-label">
				            <input class="form-check-input" type="radio" name="visibility" id="2" value="2">
				            Privé
				          </label>
				        </div>
				        <div class="form-check form-check-inline">
				          <label class="form-check-label">
				            <input class="form-check-input" type="radio" name="visibility" id="3" value="3">
				            Visible que pour mes amis
				          </label>
				        </div>
				      </div>
				    </fieldset>
				    <div class="form-group row">
						<div class="offset-sm-2 col-sm-10">
					        <button type="submit" class="btn btn-primary">Créer</button>
					    </div>
					</div>
			    
	            </div>
				
			</form>
		</div>
	</div>
	</div>



	@include('layouts.right-content')

@endsection