@extends('layouts.app')

@section('content')

	@include('layouts.left-sidenav')
	{{-- Code HTML à partir d'ici --}}

	 <div class="col-sm-6">
	 <h1 style="text-align:center">Mes événements</h1>

	 <a href="{{url('events/create')}}" ><i class="fa fa-plus-circle fw" aria-hidden="true" ></i>&nbsp; Nouveau</a>


	@foreach($events as $event)
	<div class="card" style="border:1px solid #cccccc; border-top-right-radius:0.25rem; border-top-left-radius:0.25rem; padding:10px 10px 0px 10px">
		
		<div style="width:auto; height:300px; margin:auto; float:left; padding:10px 30px 10px 10px; ">
			<div class="align-middle" style="height:auto; margin:75px 0px 75px 0px">

				<h4>{{$event->title}}</h4>
				<p style="text-align:left">Du {{\Carbon\Carbon::parse($event->beginDate)->format('d/m/Y H:m')}} au {{\Carbon\Carbon::parse($event->endDate)->format('d/m/Y H:m')}}</p>
				@if($event->media_id != null)
					 @if($event->media->type === 'image')
						<img src= "{{$event->media->path}}" style="border:1px solid black; margin-bottom:4px">
	                @endif
	            @endif
                <p style="text-align:left">{{$event->description}}</p>
				
			</div>
		</div>
			
		
	</div>
	@endforeach
	
	

	@include('layouts.right-content')

@endsection