@extends('layouts.app')

@section('content')
    
    @include('layouts.left-sidenav')

    <div class="col-sm-6">
        <create-post></create-post>
        @foreach($collections as $posts)
            @include('layouts.posts')
        @endforeach
    </div>
    @include('layouts.right-content')
@endsection
