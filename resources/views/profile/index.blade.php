@extends('layouts.app')

@section('content')

    @include('layouts.left-sidenav')
    {{-- Code HTML à partir d'ici --}}
    <div class="col-sm-6">
        <div class="card">
            <img class="card-img-top" src="http://lorempixel.com/800/200/" alt="Card image cap">
            <div class="card-block">
                <img class="card-img-top" style="position:absolute; top:90px; border:1px solid black" src="http://lorempixel.com/125/125/">
                <p class="card-text" style="font-size:40px; margin-bottom:0px;"> {{ $user->firstname }} {{ $user->lastname }} </p>
            </div>
        </div>
        <create-post></create-post>

        @include('layouts.posts')
    </div>

    @include('layouts.right-content')

@endsection