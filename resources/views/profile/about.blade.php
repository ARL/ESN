@extends('layouts.app')

@section('content')

    @include('layouts.left-sidenav')
    {{-- Code HTML à partir d'ici --}}
    <div class="col-sm-6">
        <div class="card">
            @if($user->cover)
                <img class="card-img-top" src="{{ $user->cover->path }}" alt="Image de couverture" width="800" height="300">
            @else
                <img class="card-img-top" src="{{ asset('img/cover_default.jpg') }}" alt="Image de couverture" width="800" height="200">
            @endif
            <div class="card-block">
                @if($user->avatar)
                    <img class="card-img-top" style="position:absolute; top:90px; border:1px solid white" src="{{ $user->avatar->path }}" width="125" height="125">
                @else
                    <img class="card-img-top" style="position:absolute; top:90px; border:1px solid white" src="{{ asset('img/avatar_default.jpg') }}" width="125" height="125">
                @endif
                <p class="card-text" style="font-size:40px; margin-bottom:0px;"> {{ $user->firstname }} {{ $user->lastname }} </p>
                <p class="card-text"> {{'@'.$user->username}} <br> {{ \Carbon\Carbon::parse($user->birthday)->diffInYears() }} ans </p>
                <p class="card-text" style="font-size:20px"> Date de naissance : {{ \Carbon\Carbon::parse($user->birthday)->format('d/m/Y') }}  <br> email : {{ $user->email }}  <br> Adresse : {{$user->address}} <br> Année d'étude : ING{{ $user->currentYear }}</p>

                @if (Auth::user()->id==$user->id)
                    <a href="{{url('profile/edit')}}"> <button class="btn btn-primary">Modifier</button></a>
                @endif

            </div>
        </div>
    </div>

    @include('layouts.right-content')

@endsection