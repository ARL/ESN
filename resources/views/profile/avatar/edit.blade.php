@extends('layouts.app')

@section('content')

	@include('layouts.left-sidenav')
	{{-- Code HTML à partir d'ici --}}
	<div class="col-sm-6">
		<div class="card">
		  <img class="card-img-top" src="http://lorempixel.com/800/200/" alt="Card image cap">
		  <div class="card-block">
		  	<img class="card-img-top" style="position:absolute; top:90px; border:1px solid black" src="http://lorempixel.com/125/125/">
		    <p class="card-text" style="font-size:40px; margin-bottom:0px;"> {{ $user->firstname }} {{ $user->lastname }} </p>
		    <p class="card-text"> {{ \Carbon\Carbon::parse($user->birthday)->diffInYears() }} ans
		    <p class="card-text" style="font-size:20px"> Date de naissance : {{ \Carbon\Carbon::parse($user->birthday)->format('m/d/Y') }}  <br> email : {{ $user->email }} <br> Année d'étude : ING{{ $user->currentYear }}</p>
		  </div>
		</div>
	</div>

	@include('layouts.right-content')

@endsection