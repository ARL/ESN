@extends('layouts.app')

@section('content')

    @include('layouts.left-sidenav-extern')
    {{-- Code HTML à partir d'ici --}}
    <div class="col-sm-6">
        <div class="card">
            <img class="card-img-top" src="http://lorempixel.com/800/200/" alt="Card image cap">
            <div class="card-block">
                <img class="card-img-top" style="position:absolute; top:90px; border:1px solid black" src="http://lorempixel.com/125/125/">
                <p class="card-text" style="font-size:40px; margin-bottom:0px;"> {{ $user->firstname }} {{ $user->lastname }} </p>
                <p class="card-text"> {{'@'.$user->username}} <br> {{ \Carbon\Carbon::parse($user->birthday)->diffInYears() }} ans </p>
                <p class="card-text" style="font-size:20px"> Date de naissance : {{ \Carbon\Carbon::parse($user->birthday)->format('d/m/Y') }}  <br> email : {{ $user->email }}  <br> Adresse : {{$user->address}} <br> Année d'étude : ING{{ $user->currentYear }}</p>

                @if (Auth::user()->id==$user->id)
                    <a href="{{url('profile/edit')}}"> <button class="btn btn-primary">Modifier</button></a>
                @endif

            </div>
        </div>
    </div>

    @include('layouts.right-content')

@endsection