@extends('layouts.app')

@section('content')

    @include('layouts.left-sidenav')
    {{-- Code HTML à partir d'ici --}}
    <form class="col-sm-6" method="POST" action="{{url('profile/update')}}" enctype="multipart/form-data">
        <h2>Modifier mon profil</h2>
        {{ csrf_field() }}
        <div class="form-group">
            <label for="profile_picture">Changer la photo de Profil</label>
            <input type="file" class="form-control-file" aria-describedby="coverHelp" name="profile_picture" accept="image/png,image/gif,image/jpeg">
            <small id="coverHelp" class="form-text text-muted">Pour un beau rendu 800x200 (jpg, jpeg, png, gif).</small>
        </div>
        <div class="form-group">
            <label for="profile_picture">Changer la photo de Couverture</label>
            <input type="file" class="form-control-file" name="cover_picture" accept="image/png,image/gif,image/jpeg">
            <small id="avatarHelp" class="form-text text-muted">Pour un beau rendu image de côtés identiques (jpg, jpeg, png, gif).</small>
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" name="username" placeholder="{{ $user->username }}">
        </div>
        <div class="form-group">
            <label for="firstname">Prénom</label>
            <input type="text" class="form-control" name="firstname"  placeholder="{{ $user->firstname }}">
        </div>
        <div class="form-group">
            <label for="lastname">Nom</label>
            <input type="text" class="form-control" name="lastname"  placeholder="{{ $user->lastname }}">
        </div>
        <div class="form-group">
            <label for="birthday">Date de naissance</label>
            <input type="date" class="form-control" name="birthday" value="{{ $user->birthday }}">
        </div>
        <div class="form-group">
            <label for="address">Adresse</label>
            <input type="text" class="form-control" name="address"  placeholder="{{ $user->address }}">
        </div>
        <div class="form-group">
            <label for="currentYear">Année d'étude</label>
            <select name="currentYear" class="form-control" id="currentYear">
                <option value="1" @if($user->currentYear == 1) selected @endif>ING1</option>
                <option value="2" @if($user->currentYear == 2) selected @endif>ING2</option>
                <option value="3" @if($user->currentYear == 3) selected @endif>ING3</option>
                <option value="4" @if($user->currentYear == 4) selected @endif>ING4</option>
                <option value="5" @if($user->currentYear == 5) selected @endif>ING5</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </form>

    @include('layouts.right-content')

@endsection