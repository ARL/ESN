@extends('layouts.app')

@section('content')

	@include('layouts.left-sidenav')
	{{-- Code HTML à partir d'ici --}}
	<div class="col-sm-6">
		<ul class="list-group">
			@foreach ($users as $user)
				@if ((!($user->hasBlocked(Auth::user()))) && $user!=Auth::user())
					<li class="list-group-item">
						<img src="http://placehold.it/120x120" style="border:1px solid black; margin:5px 15px 5px 5px">
						<div class="justify-content-between">
							<a class="card-title mb-1" href="{{url('wall/'.$user->id)}}"> <h2 style="color:black"> {{ $user->firstname }} {{ $user->lastname }} </h2></a>
							<p class="text-muted" style="font-size:15px">{{ '@'.$user->username }} <br> {{ $user->getMutualFriendsCount(Auth::user() )}} amis en commun </p>

							@if ($user->isFriendWith(Auth::user()))
								<button class="btn btn-secondary" style="color:#1111ee" disabled>Déjà ami</button>
								<a class="btn btn-danger" href="{{url('friend/store/'.$user->id.'/block')}}" role="button">Bloquer</a>
							@elseif ($user->hasFriendRequestFrom(Auth::user()))
								<button class="btn btn-secondary" style="color:#1111ee" disabled>En attente</button>
							@elseif ($user->isBlockedBy(Auth::user()))
								<button class="btn btn-secondary" style="color:#1111ee" disabled>Bloqué</button>
								<a class="btn btn-danger" href="{{url('friend/store/'.$user->id.'/unblock')}}" role="button">Débloquer</a>
							@elseif (!($user->isFriendWith(Auth::user())))
								<a class="btn btn-primary" href="{{url('friend/store/'.$user->id)}}" role="button">Envoyer une demande d'ami</a>
							@endif
						</div>
					</li>
				@endif
			@endforeach
		</ul>
	</div>

	@include('layouts.right-content')

@endsection