<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'ESN') }}</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#app-navbar-collapse" aria-controls="app-navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('img/brand-image.png') }}" width="30" height="30" alt="">
                </a>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        <form action="{{url('users')}}" method="GET" class="form-inline">
                            <input class="form-control mr-sm-2" type="text" placeholder="Rechercher un utilisateur" name="searchbar">
                            <button class="btn btn-default-sm" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="btn btn-default-sm" href="{{url('friend/requests')}}">
                                @if (count(Auth::user()->getFriendRequests()) == 0)
                                    <span class="badge badge-default">{{ count(Auth::user()->getFriendRequests()) }} </span>
                                @else
                                    <span class="badge badge-danger">{{ count(Auth::user()->getFriendRequests()) }} </span>
                                @endif
                                <i class="fa fa-user-plus fw"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('wall') }}">{{ Auth::user()->fullname() }}</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="{{ url('profile') }}" id="user-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cogs" aria-hidden="true"></i></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="user-options">
                                @role('admin')
                                    <a class="dropdown-item" href="{{ url('admin/dashboard') }}"><i class="fa fa-lock fw" aria-hidden="true"></i>&nbsp; Administration</a>
                                @endrole
                                <a class="dropdown-item" href="{{ url('profile/edit') }}"><i class="fa fa-pencil-square-o fw" aria-hidden="true"></i>&nbsp; Modifier mon profil</a>
                                <a class="dropdown-item" href="{{ route('logout') }}" 
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fw" aria-hidden="true"></i>&nbsp; Déconnexion
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="col-sm-4" style="position:absolute; top:80px; right:10px; z-index: 1000;">
                @include('flash::message')
            </div>
            
            <div class="content">
                <div class="container-fluid">
                    @include('layouts.errors')
                    <div class="row">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
    </body>
</html>