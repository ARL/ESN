<div class="col-sm-2">
    <div class="list-group">
        <a href="{{url('wall') . '/' . $user->id}}" class="list-group-item {{ Request::is('wall/' . $user->id)? 'active' : '' }}" ><i class="fa fa-user-circle fw" aria-hidden="true"></i>&nbsp; Chronlogie</a>
        <a href="{{ url('wall') . '/' . $user->id . '/about'}}" class="list-group-item {{ Request::is('wall/' . $user->id . '/about') ? 'active' : '' }}"><i class="fa fa-info-circle fw" aria-hidden="true"></i>&nbsp; A propos</a>
        <a href="{{url('wall') . '/' . $user->id . '/friend'}}" class="list-group-item {{ Request::is('wall/' . $user->id . '/friend') ? 'active' : '' }}" ><i class="fa fa-users fw" aria-hidden="true"></i>&nbsp; Amis</a>
        <a href="{{url('wall') . '/' . $user->id . '/events'}}" class="list-group-item {{ Request::is('wall/' . $user->id . '/events') ? 'active' : '' }}"><i class="fa fa-calendar fw" aria-hidden="true"></i>&nbsp; &Eacute;vénements</a>
        <a href="{{url('wall') . '/' . $user->id . '/albums'}}" class="list-group-item {{ Request::is('wall/' . $user->id . '/albums') ? 'active' : '' }}"><i class="fa fa-picture-o fw" aria-hidden="true"></i>&nbsp; Albums</a>
    </div>
</div>