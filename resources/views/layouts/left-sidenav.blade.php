<div class="col-sm-2">
    <div class="list-group">
        <a href="{{url('wall')}}" class="list-group-item {{ Request::is('wall') ? 'active' : '' }}"><i class="fa fa-user-circle fw" aria-hidden="true"></i>&nbsp; Chronologie</a>
        <a href="{{ url('about') }}" class="list-group-item {{ Request::is('about') ? 'active' : '' }}"><i class="fa fa-info-circle fw" aria-hidden="true"></i>&nbsp; A propos</a>
        <a href="{{url('friend')}}" class="list-group-item {{ Request::is('friend') ? 'active' : '' }}"><i class="fa fa-users fw" aria-hidden="true"></i>&nbsp; Amis</a>
        <a href="{{url('messages')}}" class="list-group-item {{ Request::is('messages') ? 'active' : '' }}"><i class="fa fa-envelope fw" aria-hidden="true" active></i>&nbsp; Messages</a>
        <a href="{{url('events')}}" class="list-group-item {{ Request::is('events') ? 'active' : '' }}"><i class="fa fa-calendar fw" aria-hidden="true"></i>&nbsp; &Eacute;vénements</a>
        <a href="{{url('albums')}}" class="list-group-item {{ Request::is('albums') ? 'active' : '' }}"><i class="fa fa-picture-o fw" aria-hidden="true"></i>&nbsp; Albums</a>
    </div>
</div>