<div class="card">
    <div class="card-block">
        <div class="media">
            <img class="d-flex mr-3" width="80" height="80" src="http://placehold.it/80x80" alt="Generic placeholder image">
            <div class="media-body">
                <h4 class="card-title">
                    {{ $post->user->firstname }} {{ $post->user->lastname }}
                    @if($post->user == Auth::user())
                        <a href="{{ url('/posts/delete/' . $post->id) }}" class="btn btn-outline-danger btn-sm float-right" role="button" data-toggle="tooltip" data-placement="top" title="Supprimer la publication"><span aria-hidden="true">&times;</span></a>
                    @endif
                    <small class="text-muted">
                        @if($post->mood){{ '- ' . $post->mood }}@endif
                        @if($post->place), à <i class="fa fa-map-marker fw text-danger" aria-hidden="true"></i> {{ $post->place }}@endif
                    </small>
                </h4>
                @if($post->date)
                    <h6 class="card-subtitle mb-2 text-muted">{{ \Carbon\Carbon::parse($post->date)->format('jS F, Y') }}</h6>
                @else
                    <h6 class="card-subtitle mb-2 text-muted">{{ $post->created_at->diffForHumans() }}</h6>
                @endif
            </div>
        </div>
        <hr>
        <p class="card-text" style="text-align: justify; text-justify: inter-word;">{{ $post->content }}</p>
        @if ($post->media)
            @if($post->media->type === 'image')
                <img class="img-fluid post-image" src="{{ $post->media->path }}" alt="">
            @elseif($post->media->type === 'video/webm' || $post->media->type === 'video/mp4')
                <video controls>
                    <source src="{{ $post->media->path }}" type="{{ $post->media->type }}">
                    Veuillez mettre à jour votre navigateur.
                </video>
            @endif
        @endif
        <hr>
        <a href="{{ url('/posts/' . $post->id . '/reaction/LIKE') }}" class="btn btn-primary btn-sm" role="button">
            <i class="fa fa-thumbs-up fa-fw" aria-hidden="true"></i>
            @php
                $likes = 0;
                foreach ($post->reactions as $reaction)
                    if($reaction->name == 'LIKE') $likes++;
                echo $likes;
            @endphp
        </a>
        <a href="{{ url('/posts/' . $post->id . '/reaction/LOVE') }}" class="btn btn-danger btn-sm" role="button">
            <i class="fa fa-heart-o fa-fw" aria-hidden="true"></i>
            @php
                $loves = 0;
                foreach ($post->reactions as $reaction)
                    if($reaction->name == 'LOVE') $loves++;
                echo $loves;
            @endphp
        </a>
        <a href="{{ url('/posts/' . $post->id . '/reaction/HAHA') }}" class="btn btn-warning btn-sm" role="button">
            <i class="fa fa-smile-o fa-fw" aria-hidden="true"></i>
            @php
                $hahas = 0;
                foreach ($post->reactions as $reaction)
                    if($reaction->name == 'HAHA') $loves++;
                echo $hahas;
            @endphp
        </a>
        <a href="{{ url('/posts/' . $post->id . '/reaction/SAD') }}" class="btn btn-secondary btn-sm" role="button">
            <i class="fa fa-frown-o fa-fw" aria-hidden="true"></i>
            @php
                $sads = 0;
                foreach ($post->reactions as $reaction)
                    if($reaction->name == 'SAD') $sads++;
                echo $sads;
            @endphp
        </a>
    </div>
</div>