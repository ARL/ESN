<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Fabien Dartigues, Anaïs Lavorel, Martin Mony">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="icon" href="favicon.ico">

    <title>ECE Social Network</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/welcome.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand"><a href="{{ url('/') }}">ESN</a></h3>
              <nav class="nav nav-masthead">
                <a class="nav-link @yield('loginActive')" href="{{ url('/login') }}">Connexion</a>
                <a class="nav-link @yield('registerActive')" href="{{ url('/register') }}">Inscription</a>
              </nav>
            </div>
          </div>
          <div class="inner cover">
            @yield('content')
          </div>

          <div class="mastfoot">
            <div class="inner">
              <p>Code::status(200) <a href="http://ece.fr">@ECE Paris</a> - <a href="#">Fabien Dartigues</a>, <a href="#">Anaïs Lavorel</a> & <a href="#">Martin Mony</a>.</p>
            </div>
          </div>

        </div>

      </div>

    </div>
  </body>
</html>
