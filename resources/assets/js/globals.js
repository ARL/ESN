// Autoresize automatique des textareas
window.jQuery.each(jQuery('textarea[data-autoresize]'), function() {
    let offset = this.offsetHeight - this.clientHeight;
    let resizeTextarea = function(el) {
        jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
    };
    jQuery(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
});

// Faire disparaître les notifications au bout de 5s
$('div.alert-important').delay(5000).fadeOut(350);

// Au click sur la vidéo on met sur pause ou sur play
$('video').click(function() {
    this.paused ? this.play() : this.pause();
});

// Activer les tooltips
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})