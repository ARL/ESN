<?php

namespace App\Http\Controllers;
use \Carbon\Carbon;


use Illuminate\Http\Request;
use \App\Models\Event;
use Auth;

class EventController extends Controller
{
    /**
     * EventController constructor.
     */
    public function __construct() {
        // Protection des routes
        $this->middleware('auth');
    }

    public function indexFutur()
    {

        $events = Event::where('user_id', '=',  Auth::user()->id)->where('beginDate','>',Carbon::now())->orderBy('beginDate','asc')->get();   
        return view('event.event', compact('events'));
    }

    public function indexPast()
    {

        $events = Event::where('user_id', '=',  Auth::user()->id)->where('beginDate','<',Carbon::now())->orderBy('beginDate','desc')->get();   
        return view('event.event', compact('events'));
    }

    public function create() {
        return view('event.create');
    }

    public function store(Request $request) {
        $title = null;
        $content = null;
        $visibility = 1;
        $address =null;
        $media=null;
        $beginDate = $request->beginDate;
        $endDate = null;

        // Get the user
        $user = Auth::user();

        if ($request->has('title')) {
            $title = $request->title;
        }

        if ($request->has('content')) {
            $content = $request->content;
        }
        

        if ($request->has('address')) {
            $address = $request->address;
        }

        $beginDate = Carbon::parse( $request->beginDate); 
        

        if ($request->has('address')) {
            $endDate = $request->endDate;
        }
        if ($request->has('visibility')) {
            $visibility = $request->visibility;
        }
        

        // The media
        if ($request->hasFile('media') && $request->file('media')->isValid()) {
            $uploadedFile = $request->file('media');

            $filename = time() . '.' . $uploadedFile->getClientOriginalExtension();
            $mimeType = explode('/', $uploadedFile->getClientMimeType());
            $type = $mimeType[0];

            $uploadedFile->move(public_path() . '/uploads/events/'.$filename);

            $media = \App\Models\Media::create([
                'type' => $type,
                'path' => $filename,
                'user_id' => Auth::user()->id
            ]);
        }

        // creer l event
        Auth::user()->events()->create([
            'title' => $title ? $title : "Unknow",
            'content' => $content  ,
            'address' => $address,
            'visibility_id' => $visibility ,
            'media_id' => $media,
            'beginDate'=> $beginDate,
            'endDate'=> $endDate,
             
        ]);

        // Envoyer un message flash à l'utilisateur
        flash('Votre événement a bien été créé.', 'success')->important();
        
        return redirect('events');
    }
        

    /**
     * Affiche l'évènement ayant pour id $id
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
    	$event = Event::find($id);

    	if($event) {
    	    return view('event.event', compact('event'));
        }
    }
    
}
