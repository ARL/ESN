<?php

namespace App\Http\Controllers\Album;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \App\Models\Album;
use \App\Models\AlbumMedia;
use \App\Models\Media;

class AlbumsController extends Controller
{
    //
    public function index() {
    	$user=Auth::user();

    	$albums=Album::where('user_id', '>=', $user->id)->get();

    	return view('album.index', compact('albums'));

    }

    public function show($id) {
    	$album=Album::find($id);
    	$albummedia=AlbumMedia::where('album_id', '>=', $id)->get();

    	$medias=[];
    	foreach($albummedia as $am)
    	{
    		$media=Media::find($am->media_id);
    		$medias[]=$media;
    	}

    	// qd j avais un dd ici ca marchait genre lt tableau medias il avait 1 media, celui qu icorrespont a l'album 1
    	// apres j ai testé tout le long avec des dd et il récupérait bien

		return view('album.show', compact('album','medias'));
    }

     public function create() {
        return view('album.create');
    }
}
