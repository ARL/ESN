<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\User;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct() {
        // Protection des routes
        $this->middleware('auth');
    }

    /**
     * Recherche d'un utilisateur
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function fetch(Request $request){
    	if ($request->has('searchbar'))	
    	{
    		$field = $request->input('searchbar');
    		$users=User::where('firstname', 'LIKE', $field.'%')->orWhere('lastname', 'LIKE', $field.'%')->orWhere('username', 'LIKE', $field.'%')->orderBy('lastname')->get();
    		return view('users.fetch_result',compact('users'));
    	} else
    	{
    		return redirect('home');
    	}
    }
}
