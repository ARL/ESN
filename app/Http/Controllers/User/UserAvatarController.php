<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class UserAvatarController extends Controller
{
    /**
     * Update the avatar for the user.
     *
     * @param  Request  $request
     */
    public function update(Request $request) {

    	if($request->hasFile('avatar_url')) {
    		
    		$user = Auth::user();

    		$avatar = $request->file('avatar_url');

    		$path = $avatar->store('avatars');

    		$user->avatar_url = $path;

    		$user->save();
    	}
    }

    /**
     * Update the avatar for the user.
     *
     * @param  Request  $request
     */
    public function edit() {
        $user=Auth::user();
    	return view('profile.avatar.edit', compact('user'));
    }
}
