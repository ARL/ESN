<?php

namespace App\Http\Controllers\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Post;

class PostsController extends Controller
{
    /**
     * PostsController constructor.
     */
    public function __construct() {
        // Protection des routes
        $this->middleware('auth');
    }

    /**
     * Publier un post
     * Vérification si au moins une photo, vidéo ou content
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        // Initialisation
        $mood = "";
        $place = "";
        $activity = "";
        $date = "";
        $content = "";
        $media = null;


        // Get the user
        $user = Auth::user();

        if ($request->has('mood'))
            $mood = $request->mood;

        if($request->has('date'))
            $date = $request->date;

        if($request->has('place'))
            $place = $request->place;

        if($request->has('wayd'))
            $activity = $request->wayd;

        if($request->has('post_content'))
            $content = $request->post_content;

        // The media
        if ($request->hasFile('media') && $request->file('media')->isValid()) {
            $uploadedFile = $request->file('media');

            $filename = time() . '.' . $uploadedFile->getClientOriginalExtension();
            $mimeType = explode('/', $uploadedFile->getClientMimeType());
            $type = $mimeType[0];

            if($type === 'video') {
                $type = $type . '/' . $uploadedFile->extension();
            }

            $uploadedFile->move(public_path() . '/uploads/posts/', $filename);

            $media = \App\Models\Media::create([
                'type' => $type,
                'path' => '/uploads/posts/' . $filename,
                'user_id' => $user->id
            ]);
        }

        /*
         * Si l'utilisateur n'a posté ni photo, ni vidéo, ni message on le redirige
         */
        if($content === "" && $media === null) {
            flash("Vous devez au moins entrer un message ou insérer une photo/vidéo.", 'danger')->important();
            return redirect('home');
        }

        $post = $user->posts()->create([
            'content' => $content ? $content : null,
            'mood' => $mood ? $mood : null,
            'date' => $date ? $date : null,
            'place' => $place ? $place : null,
            'activity' => $activity ? $activity : null,
            'visibility_id' => 1,
            'media_id' => $media ? $media->id : null
        ]);

        // Envoyer un message flash à l'utilisateur
        flash('Votre publication a bien été publiée.', 'success')->important();

        return redirect('posts/' . $post->id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete ($id) {

        $post = Post::find($id);

        if($post) {
            if($post->user->id === Auth::user()->id) {
                $post->delete();
                flash('Votre publication a bien été supprimée', 'success')->important();
                return redirect('wall');
            } else {
                return redirect('home');
            }
        } else {
            return redirect('home');
        }
    }

    public function show ($id) {

        $post = Post::find($id);

        if($post) {
            $post->load('user', 'media', 'reactions');
            return view('posts.show', compact('post'));
        } else {
            return redirect()->back();
        }

    }
}
