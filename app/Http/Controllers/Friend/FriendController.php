<?php

namespace App\Http\Controllers\Friend;

use App\Http\Controllers\Controller;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Http\Request;
use \App\User;
use Auth;


class FriendController extends Controller
{
    /**
     * FriendController constructor.
     */
    public function __construct() {
        // Protection des routes
        $this->middleware('auth');
    }

    /**
     * Affiche la liste des amis
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index () {
        $user = Auth::user();
        $friends = $user->getFriends()->load('cover', 'avatar');

        return view('friend.index', compact('friends'));
    }

    /**
     * Permet de créer un relation d'amitié
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store ($id) {
    	$user = User::find($id);
    	if ($user) {
    		Auth::user()->befriend($user);
    		flash("La demande a bien été envoyée", 'success')->important();
    	} else {
    		flash("Utilisateur introuvable.", 'danger')->important();
    	}

    	return redirect('home');
    }

    /**
     * Accepter une demande d'ami
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function accept ($id) {
        $user = User::find($id);
        $auth = Auth::user();
        if ($user)
        {
            if($auth->hasFriendRequestFrom($user)) {
                $auth->acceptFriendRequest($user);
                flash("La demande a bien été acceptée", 'success')->important();
            } else {
                return redirect('home');
            }
        } else {
            flash("Utilisateur introuvable.", 'danger')->important();
        }

        return redirect('home');
    }

    /**
     * Refuse une demande d'ami
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deny ($id) {
        $user = User::find($id);
        $auth = Auth::user();

        if ($user) {
            if($auth->hasFriendRequestFrom($user)) {
                Auth::user()->denyFriendRequest($user);
                flash("La demande a bien été refusée.", 'success')->important();
            } else {
                return redirect('home');
            }
        } else {
            flash("Utilisateur introuvable.", 'danger')->important();
        }

        return redirect('home');
    }

    /**
     * Supprime un utilisateur de sa liste d'amis
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function remove ($id) {

        $user = User::find($id);
        $auth = Auth::user();

        if ($user) {
            if($user->isFriendWith($auth)) {
                $auth->unfriend($user);
                flash("Vous avez bien retiré l'utilisateur de votre liste d'amis", 'success')->important();
            } else {
                return redirect('home');
            }
        } else {
            flash("Utilisateur introuvable.", 'danger')->important();
        }

        return redirect('home');
    }

    /**
     * Bloque un ami
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function block ($id) {
        $user = User::find($id);
        $auth = Auth::user();
        if ($user) {
            if($auth->isFriendWith($user)) {
                $auth->blockFriend($user);
                flash("L'utilisateur a été bloqué avec succès.", 'success')->important();
            } else {
                return redirect('home');
            }
        } else {
            flash("Utilisateur introuvable.", 'danger')->important();
        }


        return redirect('home');
    }

    /**
     * Débloque un ami
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function unblock ($id) {
        $user = User::find($id);
        $auth = Auth::user();
        if ($user)
        {
            if($auth->isFriendWith($user)) {
                $auth->unblockFriend($user);
                flash("L'utilisateur a été débloqué avec succès.", 'success')->important();
            } else {
                return redirect('home');
            }
        } else {
            flash("Utilisateur introuvable.", 'danger')->important();
        }

        return redirect('home');
    }

}
