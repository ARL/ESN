<?php

namespace App\Http\Controllers\Friend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use \App\User;

class FriendRequestController extends Controller
{
    /**
     * FriendRequestController constructor.
     */
    public function __construct() {
        // Protection des routes
        $this->middleware('auth');
    }

    /**
     * Affiche la liste des demandes d'amis
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
    	$user = Auth::user();
    	$requests = $user->getFriendRequests();
    	//dd($requests);
    	$senders = [];

    	foreach ($requests as $request)
    	{
    		$sender = User::find($request->sender_id);
    		$senders[] = $sender;
    	}

    	return view('friend.requests', compact('senders'));
    }
}
