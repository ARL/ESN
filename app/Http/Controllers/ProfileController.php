<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\User;

class ProfileController extends Controller
{
    /**
     * ProfileController constructor.
     */
    public function __construct() {
        // Protection des routes
        $this->middleware('auth');
    }

    /**
     * Affiche le profil de l'utilisateur connecté
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
	public function index() {
	    $user = Auth::user();
	    $posts = $user->posts()->with('media', 'reactions')->get();

	    return view('profile.index', compact(['user', 'posts']));
    }

    /**
     * Affiche le profil d'un utilisateur
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id) {

        $user = User::find($id);
        if($user) {
            $posts = $user->posts()->with('media', 'reactions', 'avatar', 'cover')->get();
            return view('profile.show', compact('user', 'posts'));
        } else {
            return redirect('home');
        }

    }

    /**
     * About me
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about() {
        $user = Auth::user();

        $user->load('avatar', 'cover');

        return view('profile.about', compact('user'));
    }

    /**
     * About someone
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function aboutSomeone($id) {
        $user = User::find($id);

        if($user) {
            $user->load('avatar', 'cover');
            return view('profile.about-someone', compact('user'));
        } else {
            return  redirect('/home');
        }

    }

    /**
     * Affiche la page d'édition d'un utilisateur
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit() {
        $user = Auth::user();
        $user->load('avatar', 'cover');
        return view('profile.edit', compact('user'));       
    }

    public function update(Request $request) {
        
//        $this->validate($request, [
//            /*
//            'ProfilePhoto'=>'mimetypes:jpeg,png,gif|file',
//            'CoverPhoto'=>'mimetypes:jpeg,png,gif|file',
//            */
//            'firstname' => 'max:255',
//            'lastname' => 'max:255',
//            'birthday' => 'date',
//            'address' => 'max:255',
//            /*
//            */
//        ]);
        
        $user = Auth::user();
        if($request->has('firstname'))
            $user->firstname = $request->firstname;

        if($request->has('lastname'))
            $user->lastname = $request->lastname;

        if($request->has('birthday'))
            $user->birthday = $request->birthday;

        if($request->has('address'))
            $user->address = $request->address;

        if($request->has('username'))
            $user->username = $request->username;

        if($request->has('currentYear'))
            $user->currentYear = $request->currentYear;

        if ($request->hasFile('profile_picture') && $request->file('profile_picture')->isValid()) {
            $uploadedFile = $request->file('profile_picture');

            $filename = time() . '.' . $uploadedFile->getClientOriginalExtension();
            $mimeType = explode('/', $uploadedFile->getClientMimeType());
            $type = $mimeType[0];

            $uploadedFile->move(public_path() . '/uploads/avatars/', $filename);

            $avatar = $user->avatar()->create([
                'type' => $type,
                'path' => '/uploads/avatars/' . $filename,
                'user_id' => $user->id
            ]);
            $user->avatar_id = $avatar->id;
        }

        if ($request->hasFile('cover_picture') && $request->file('cover_picture')->isValid()) {
            $uploadedFile = $request->file('cover_picture');

            $filename = time() . '.' . $uploadedFile->getClientOriginalExtension();
            $mimeType = explode('/', $uploadedFile->getClientMimeType());
            $type = $mimeType[0];

            $uploadedFile->move(public_path() . '/uploads/covers/', $filename);

            $cover = $user->cover()->create([
                'type' => $type,
                'path' => '/uploads/covers/' . $filename,
                'user_id' => $user->id
            ]);

            $user->cover_id = $cover->id;
        }

        $user->save();

        return redirect('about');

    }
}
