<?php

namespace App\Http\Controllers\Reaction;

use Auth;
use App\Models\Post;
use App\Models\Reaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Flysystem\Exception;

class ReactionsController extends Controller
{
    /**
     * ReactionController constructor.
     */
    public function __contruct() {
        $this->middleware('auth');
    }

    /**
     * Reagir à un post
     * @param $id
     * @param $reaction
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store ($id, $reaction) {

        $reactions = collect([
            'LIKE',
            'LOVE',
            'SAD',
            'HAHA',
        ]);

        if($reactions->containsStrict($reaction)) {
            $post = Post::find($id);
            $user = Auth::user();
            if($post) {
                $reactionExists = $user->reactions()->where('post_id', '=', $id)->get();

                if($reactionExists->count() < 1) {
                    Reaction::create([
                        'name' => $reaction,
                        'post_id' => $id,
                        'user_id' => $user->id
                    ]);
                } else {
                    flash('Vous avez déjà réagi à cette publication !', 'danger')->important();
                }

            }
        }

        return redirect('posts/' . $id);



    }
}
