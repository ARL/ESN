<?php

namespace App\Http\Controllers;

use App\Models\Media;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use \App\Models\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard
     * Fil d'actualité de l'utilisateur
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        // On récupère les amis
        $friends = $user->getFriends();
        $collections = collect();

        // Pour chaque amis
        foreach($friends as $friend) {
            // On récupère les posts publiés des dernières 24h
            $friendPosts = $friend->posts()
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->get();

            $friendPosts->load('user', 'media', 'reactions');

            $collections->push($friendPosts);
        }
        $collections->collapse();
        // On ordonne les posts
        $collections->sortByDesc('created_at');

        return view('home', compact('collections'));
    }

    public function show($id) {
        $posts = Post::find($id);
        if($posts) {
            $posts->load('user', 'media', 'reactions');
            return view('posts.show', compact('posts'));
        } else {
            return redirect('home');
        }
    }
}
