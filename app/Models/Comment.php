<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'content', 
    ];


    public function user(){
    	return $this->belongTo('\App\User');
    }

    /**
     * @return the post who is linked to media
     */
    public function post(){
    	return $this->belongTo('\App\Models\Post');
    }



}
