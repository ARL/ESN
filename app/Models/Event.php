<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\User;


class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'beginDate', 'endDate', 'address', 'visibility_id', 'media_id',
    ];

    protected $dates = [
    	'beginDate', 'endDate',
    ];

    // make a relation between users and Event
    public function user(){
    	return $this->belongsTo('\App\User');
    }

    //make a relation between event and media
    public function media(){
    	return $this->belongsTo('\App\Models\Media');
    }

    public function visibility(){
        return $this->belongsTo('\App\Models\Visibility');
    }

    public function groups(){
        return $this->belongsToMany('\App\Models\Group', 'event_groups', 'event_id', 'group_id');
       
    }
}
