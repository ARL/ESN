<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];


	// make a relation between users and Event
    public function users(){
    	return $this->belongsToMany('\App\User', 'group_user', 'group_id', 'user_id');
    }

    public function visibility(){
    	return $this->belongsTo('\App\Models\Visibility');
    }

    /**
     * @return the posts of a group
     */
    public function posts(){
    	return $this->belongsToMany('\App\Models\Post','post_groups', 'group_id', 'post_id');
    }

    /**
     * @return the event that a group had acces
     */
    public function events(){
    	return $this->belongsToMany('\App\Models\Event', 'event_groups', 'group_id', 'event_id');
    }

    /**
     * @return the messages of a group
     */
    public function messages(){
        return  $this->hasMany('\App\Models\Message', 'group_id');
    }

    //Albums
}
