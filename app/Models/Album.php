<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = ['title', 'description',
    ];

    /**
     * @return the user who had this album
     */
    public function user(){
    	return $this->belongsTo('\App\User');
    }

    //Erreur de rendu: renvoie une liste vide :o
    public function visibility(){
    	return $this->belongsTo('\App\Models\Visibility');
    }

    /**
     * @return the medias contains into this album
     */
    public function medias(){
    	return $this->belongsToMany('\App\Models\Media', 'album_medias', 'album_id', 'media_id' );
    }
}
