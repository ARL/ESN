<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public $table= 'medias';
	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'path', 'user_id'
    ];


	// make a relation between users and Event
    public function user(){
    	return $this->hasOne('\App\User');
    }

    public function post(){
        return $this->hasOne('\App\Models\Post');
    }

    // To check
    public function albums(){
        return $this->belongsToMany('\App\Models\Media', 'album_medias', 'media_id', 'album_id' );
    }

}
