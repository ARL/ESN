<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'content', 'date', 'place', 'activity', 'mood', 'media_id', 

    ];

    protected $date = [ 'date',
    ];


    // a post belong to one unique user
    public function user(){
        return $this->belongsTo('\App\User');
    }

    //a post have only onve visibilitie
    public function visibility(){
        return $this->belongsTo('\App\Models\Visibility');
    }

     public function media(){
        return $this->belongsTo('\App\Models\Media');
    }


    //a post can concernes groups not implemented
    /*public function groups(){
        return $this->belongsToMany('\App\Models\Group');
    }
    */

    public function reactions(){
        return $this->hasMany('\App\Models\Reaction', 'post_id' );
    }

    public function comments(){
        return $this->hasMany('\App\Models\Comment', 'post_id');
    }


}
