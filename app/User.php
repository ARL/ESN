<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;
use Spatie\Permission\Traits\HasRoles;
use Hootlex\Friendships\Traits\Friendable;
use Auth;

class User extends Authenticatable
{
    use Notifiable, CrudTrait, HasRoles, Friendable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'firstname', 'lastname', 'email', 'password', 'username', 'birthday', 'gender',  'cellphone', 'address', 'avatar_id', 'avatarResized_id', 'cover_id', 'coverResized_id',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Return the user's fullname
     *
     * @return string
     */
    public function fullname(): string {
        $user = Auth::user();
        return "$user->firstname $user->lastname";
    }

    // Recupérer tout les events
    public function events(){
        return $this->hasMany('App\Models\Event');
    }

    //

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function avatar(){
        return $this->belongsTo('App\Models\Media', 'avatar_id');
    }

    public function cover(){
        return $this->belongsTo('App\Models\Media', 'cover_id');
    }

    public function bcoverResized(){
        return $this->belongsTo('App\Models\Media',  'coverResized_id');
    }

    public function avaterResized(){
        return $this->belongsTo('App\Models\Media',  'avatarResized_id');
    }

    public function posts(){
        return $this->hasMany('App\Models\Post');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }

    public function group(){
        return $this->belongsToMany('\App\Group');
    }

    public function reactions() {
        return $this->hasMany('App\Models\Reaction');
    }


    //Album

}
